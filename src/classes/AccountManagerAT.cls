public class AccountManagerAT implements IManager
{
    public static void afterInsert(List<sObject> newObjList)
    {
        if (!newObjList.isEmpty()){
            List<Account> accInsList = (List<Account>) newObjList;
            GLOW_r1_TriggerUtilityClass.insertDealerLienholder = TRUE;
            Glow_r1_populateDealerLienholder.insertDealerLienholder(accInsList);
        }
    }
	public static void beforeInsert(List<sObject> newObjList){}
	public static void beforeUpdate(List<sObject> newObjList, Map<Id,sObject> oldObjMap){}
	public static void beforeDelete(){}
	public static void afterUpdate(List<sObject> newObjList, Map<Id,sObject> oldObjMap){}
	public static void afterDelete(){}
	public static void afterUndelete(){}
}