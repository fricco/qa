@isTest
private class AccountDAOTest {

    private static  testMethod void testGetAllVisibilePortalAccountsInfo()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Contact testContact = TWGTestFactory.createContact();
        testContact.AccountId = testAccount.Id;
        insert testContact;

        Profile testProfile = [select Id from Profile where Name = 'Portfolio - Dealer Admin'];

        User testPortalUser = TWGTestFactory.createPortalUser(testProfile.Id, testContact.Id, testProfile.Id);
        testPortalUser.PortalRole = 'Manager';
        insert testPortalUser;

        test.startTest();
        List<Account> results = AccountDAO.getAllVisibilePortalAccountsInfo();
        test.stopTest();
        // System.AssertEquals(results.size(), 1);     // For some reason the isPartner = true is not working in the unit tests
    }

    private static  testMethod void testGetAccountsByIds()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Set<Id> ids = new Set<Id>();
        ids.add(testAccount.Id);

        test.startTest();
        Map<Id,Account> results = AccountDAO.getAccountsByIds(ids);
        test.stopTest();
        System.AssertEquals(results.size(), 1);     
    }

    private static  testMethod void testGetAccountsByName()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        test.startTest();
        List<Account> results = AccountDAO.getAccountsByName(testAccount.Name);
        test.stopTest();
        System.AssertEquals(results.size(), 1);     
    }

    private static  testMethod void testGetAccountClassificationInfo()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        test.startTest();
        Account results = AccountDAO.getAccountClassificationInfo(testAccount.Id);
        test.stopTest();
        // System.AssertEquals(results.size(), 1);     
        System.AssertEquals(results.Id, testAccount.Id);     
    }

    private static  testMethod void testGetAccounts()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Set<Id> ids = new Set<Id>();
        ids.add(testAccount.Id);

        test.startTest();
        List<Account> results = AccountDAO.getAccounts(testAccount.Name);
        test.stopTest();
        System.AssertEquals(results.size(), 1);        
    }

    private static  testMethod void testGetAccountsSearchLookup()
    {
        AccountDAO accountDAO = new AccountDAO();
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Set<Id> ids = new Set<Id>();
        ids.add(testAccount.Id);

        test.startTest();
        List<Account> results = accountDAO.getAccountsSearchLookup(testAccount.Name, testAccount.BillingCity, testAccount.BillingState, testAccount.BillingPostalCode, '','','', testAccount.AccountNumber);
        test.stopTest();
        System.AssertEquals(results.size(), 1);      
    }

    private static  testMethod void testgetAccountWithTaxFields()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        test.startTest();
        Account results = AccountDAO.getAccountWithTaxFields(testAccount.Id);
        test.stopTest();
        System.AssertEquals(results.Id, testAccount.Id);     
    }

    private static  testMethod void testcpmGetQA()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Set<String> ids = new Set<String>();
        ids.add(testAccount.Id);

        test.startTest();
        List<Account> results = AccountDAO.cpmGetQA(ids);
        test.stopTest();
        System.AssertEquals(results.size(), 0);      
    }

    private static  testMethod void testAdjMgrGetEmail()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        Set<String> ids = new Set<String>();
        ids.add(testAccount.Name);

        test.startTest();
        List<Account> results = AccountDAO.adjMgrGetEmail(ids);
        test.stopTest();
        System.AssertEquals(results.size(), 1);     
    }

    private static  testMethod void testgetAccountById()
    {
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        insert testAccount;

        test.startTest();
        Account results = AccountDAO.getAccountById(testAccount.Id);
        test.stopTest(); 
        System.AssertEquals(results.Id, testAccount.Id);     
    }

    private static  testMethod void testgetAccountsByNameOrAccountNumberWithRecordType()
    {
        RecordType testRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'PORTFOLIO'];

        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        testAccount.RecordTypeId = testRecordType.Id;
        insert testAccount;

        test.startTest();
        List<Account> results = AccountDAO.getAccountsByNameOrAccountNumberWithRecordType(testAccount.Name, testAccount.AccountNumber, 'PORTFOLIO');
        test.stopTest();
        System.AssertEquals(results.size(), 1);     
    }

    private static  testMethod void testHasAccessToMoreThanOneAccountByRecordTypeFalse()
    {
        RecordType testRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'PORTFOLIO'];

        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        testAccount.RecordTypeId = testRecordType.Id;
        insert testAccount;

        test.startTest();
        Boolean result = AccountDAO.hasAccessToMoreThanOneAccountByRecordType('PORTFOLIO');
        test.stopTest();
        System.AssertEquals(result, false);     
    }

    private static  testMethod void testHasAccessToMoreThanOneAccountByRecordTypeTrue()
    {
        RecordType testRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'PORTFOLIO'];

        Account testAccount = TWGTestFactory.createAccount();
        testAccount.BillingStreet = 'Test Circle';
        testAccount.BillingCity = 'Test City';
        testAccount.BillingState = 'IL';
        testAccount.BillingPostalCode = '60563';
        testAccount.BillingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        testAccount.RecordTypeId = testRecordType.Id;
        insert testAccount;

        Account testAccount2 = TWGTestFactory.createAccount();
        testAccount2.BillingStreet = 'Test Circle';
        testAccount2.BillingCity = 'Test City';
        testAccount2.BillingState = 'IL';
        testAccount2.BillingPostalCode = '60563';
        testAccount2.BillingCountry = 'USA';
        testAccount2.Name = 'TEST03';        
        testAccount2.AccountNumber = 'TEST04';
        testAccount2.RecordTypeId = testRecordType.Id;
        insert testAccount2;

        test.startTest();
        Boolean result = AccountDAO.hasAccessToMoreThanOneAccountByRecordType('PORTFOLIO');
        test.stopTest();
        System.AssertEquals(result, true);     
    }
    private static  testMethod void testgetWarehouseAccounts()
    {
        RecordType testRecordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName= :AccountConstants.PARTNER_WAREHOUSE_RECORD_TYPE_DEV_NAME];
        Account testAccount = TWGTestFactory.createAccount();
        testAccount.ShippingStreet = 'Test Circle';
        testAccount.ShippingCity = 'Test City';
        testAccount.ShippingState = 'IL';
        testAccount.ShippingPostalCode = '60563';
        testAccount.ShippingCountry = 'USA';
        testAccount.Name = 'TEST01';        
        testAccount.AccountNumber = 'TEST02';
        testAccount.Territories_Serviced__c ='United States';
        testAccount.Status__c = Label.Site_Warehouse_Status;
        testAccount.RecordTypeId = testRecordType.Id;
        insert testAccount;
        test.startTest();
        List<Account> resultsList = AccountDAO.getWarehouseAccounts(AccountConstants.PARTNER_WAREHOUSE_RECORD_TYPE_DEV_NAME,'United States',Label.Site_Warehouse_Status);
        test.stopTest(); 
        System.AssertEquals(resultsList.get(0).Id, testAccount.Id);     
    }
    
    private static testMethod void testRetrieveAccountsBySchedulingTypeReturnsFreeStandingWarrantyAccountsWithBiWeeklySchedulingType() {
        List<Account> accountList = new List<Account>();  
        
        for(Integer i = 0 ; i<5 ; i++) {
            Account testAccount = new Account();
            testAccount = TWGTestFactory.createAccount();
            accountList.add(testAccount);
        }
        
        accountList[0].Billing_Schedule_Type__c = AccountConstants.ACCOUNT_BILLING_SCHEDULE_TYPE_BIWEEKLY;
        accountList[0].ISFSW__c = true;
        accountList[1].Billing_Schedule_Type__c = AccountConstants.ACCOUNT_BILLING_SCHEDULE_TYPE_MONTHLY;
        accountList[1].ISFSW__c = true;
        accountList[2].Billing_Schedule_Type__c = AccountConstants.ACCOUNT_BILLING_SCHEDULE_TYPE_MONTHLY;
        accountList[2].ISFSW__c = true;
        
        insert accountList;
        
        Set<String> schedulingTypeSet = new Set<String>();
             
        schedulingTypeSet.add(AccountConstants.ACCOUNT_BILLING_SCHEDULE_TYPE_BIWEEKLY);
        
        AccountDAO accountDAOInstance = new AccountDAO();
        
        Test.startTest();
            List<Account> resultAccountList = accountDAOInstance.retrieveFreeStandingWarrantyAccountsBySchedulingType(schedulingTypeSet);
        Test.stopTest();
        
        System.assertEquals(1, resultAccountList.size(), 'Retrieve only Free Standing Warranty Accounts with BiWeekly Scheduling type');     
    }
	
    private static testMethod void testGetAccountByAddressAndRecordType() {
        RecordType testRecordType = [SELECT Id, DeveloperName
                                     FROM RecordType 
                                     WHERE DeveloperName =: AccountConstants.RECORD_TYPE_SPH AND SObjectType = 'Account'
                                     AND IsPersonType = true];
        Account testAccount = new Account(FirstName = 'test', LastName = 'Test', PersonMailingStreet = 'street1',
            PersonMailingPostalCode = '302012', PersonMailingCity = 'testCity', PersonMailingState = 'Rajasthan',
            PersonMailingCountry = 'India', recordTypeId = testRecordType.Id);
        insert testAccount;
        
        Test.startTest();
            List<Account> lstCon = AccountDAO.getAccountByAddressAndRecordType(testAccount.PersonMailingStreet, 
                testAccount.PersonMailingPostalCode, testAccount.PersonMailingCity, testAccount.PersonMailingState, 
                testAccount.PersonMailingCountry, testRecordType.DeveloperName);
        Test.stopTest();
        
        System.assertEquals(1, lstCon.size(), 'One Account will be recieved');
    }
    
    private static testMethod void testGetPersonAccountDetailById() {
        RecordType testRecordType = [SELECT Id, DeveloperName
                                     FROM RecordType 
                                     WHERE DeveloperName =: AccountConstants.RECORD_TYPE_SPH AND SObjectType = 'Account'
                                     AND IsPersonType = true];
        Account testAccount = new Account(FirstName = 'test', LastName = 'Test', PersonMailingStreet = 'street1',
            PersonMailingPostalCode = '302012', PersonMailingCity = 'testCity', PersonMailingState = 'Rajasthan',
            PersonMailingCountry = 'India', recordTypeId = testRecordType.Id);
        insert testAccount;
        
        Test.startTest();
            Account testPersonAccount = AccountDAO.getPersonAccountDetailById(testAccount.Id);
        Test.stopTest();
        
        System.assertEquals(testPersonAccount.Id, testAccount.Id, 'One Account will be recieved');
    }
}